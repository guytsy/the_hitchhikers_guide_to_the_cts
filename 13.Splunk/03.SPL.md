### Goals 
The trainee will experiment with using and getting familiar with Splunk and SPL

### Tasks
- What is an event?
- What are the default fields in each event?
- How do you search for events in the most simplest way?
- What are the differences between the search modes? When would you use each one?
- How can you limit the search for a specific time in the past?
- Create a dashboard that consists of a timechart panel that show storage percentage over time.
- Add an input to your dashboard for the span of the timechart from the previous task.
- Add a base search to your dashboard and 2 more panels that uses the base search with visualzations of your choise.
- Add to your dashboard one panel that uses a macro.
- Create a report and use it in your dashboard.
- Create 3 alerts with 3 different severities. Show the triggered alerts in your dashboard.
- Create a drilldown to open the search of the panel you are clicking on.
