# Storage

### Storage media
- HDD
- SSD
- NVMe
- SATA
- M.2
- PCIe

### Partitions
- Partition types (Primary and Extended)
- `fdisk`
- `parted`
- `partprobe`
- `partx`
- GPT
- MBR

### Filesystems
- Types of filesystems

### Mounting
- `mount`
- /etc/fstab

### RAIDs
- All types of RAIDs
- Hardware RAID
- Software RAID

### LVM
- All lvm
